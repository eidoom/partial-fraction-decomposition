(* ::Package:: *)

<<FiniteFlow`


FindIndependentFactorsNumerical[expr_]:=Module[{exprfl,factors,varshere,unknowns,eqs,dummygraph,learn,indepfactors,
   indepfactorsasso,factoredexpr},

  exprfl = If[Head[expr]===List, FactorList/@expr, {FactorList[expr]}];

  factors = SortBy[Union[DeleteCases[Flatten[exprfl],a_/;Variables[a]==={}]],LeafCount]; 
  varshere = Variables[factors];
       
  unknowns = Array[ccc,Length[factors]];
  eqs = DeleteCases[Table[unknowns . D[Log[factors],vvv],{vvv,varshere}],0];
  eqs = Flatten[eqs /. 
      Table[Thread[varshere->RandomInteger[{10^3,10^6},Length[varshere]]],Length[factors]]];
  
  FFNewGraph[dummygraph];
  FFAlgDenseSolver[dummygraph,solver,{},{},eqs==0//Thread,unknowns];
  FFGraphOutput[dummygraph,solver];
  learn = FFDenseSolverLearn[dummygraph,unknowns];
  FFDeleteGraph[dummygraph];
  indepfactors = Union@@({"DepVars","ZeroVars"}/.learn) /. ccc[k_]:>k;
  indepfactors = factors[[indepfactors]];
  indepfactors = Thread[Array[y,Length[indepfactors]]->indepfactors];
  
  indepfactorsasso = Association[Join[indepfactors /. Rule[a_,b_]:>Rule[b,a],
        Thread[Factor[-indepfactors[[All,2]]]->-indepfactors[[All,1]]] ]];
  
  factoredexpr = Table[ ff[[1,1]]^ff[[1,2]]*Times@@Map[Power[indepfactorsasso[#[[1]]],#[[2]]]&, Rest[ff]],{ff,exprfl}];
  
  Return[{factoredexpr,indepfactors}]
];


Options[GetSlice] = {"NThreads"->Automatic, "PrimeNo"->0};
GetSlice[graphin_,varsin_,OptionsPattern[]]:=Module[{graphslice,tt,yy2tt,res,nthreads},
    nthreads = If[TrueQ[OptionValue["NThreads"]==Automatic],FFAutomaticNThreads[],OptionValue["NThreads"]];
    nthreads = If[EvenQ[nthreads],nthreads,nthreads-1];  (* round to even *)
    FFNewGraph[graphslice];
    FFGraphInputVars[graphslice,ttin,{tt}];
    yy2tt = Table[RandomPrime[{10^4,10^8}],{ii,varsin}]+Table[RandomPrime[{10^7,10^8}],{ii,varsin}]*tt;
    FFAlgRatFunEval[graphslice,yy2ttnode,{ttin},{tt},yy2tt];
    FFAlgSimpleSubgraph[graphslice,slicecoeffs,{yy2ttnode},graphin];
    FFGraphOutput[graphslice,slicecoeffs];
    res = FFParallelReconstructUnivariateMod[graphslice,{tt},
      {"StartingPrimeNo"->OptionValue["PrimeNo"], "PrintDebugInfo"->0,
       "NThreads"->nthreads, 
       "MaxPrimes"->200,
       "MinDegree"->Max[{nthreads/2-3,2}],
       "DegreeStep"->nthreads/2,
       "MaxDegree"->300}];
    FFDeleteGraph[graphslice];
    Return[{Rule@@@Transpose@({varsin, yy2tt}),res} /. tt->xx];
  ];


GetDegree[expr_]:=deg[Exponent[Numerator[expr],xx],Exponent[Denominator[expr],xx]]
GetDegreeMax[expr_List] := Module[{degs,maxnum,maxden},
  degs = GetDegree/@expr;
  maxnum = Max@degs[[;;,1]];
  maxden = Max@degs[[;;,2]];
  Return[{maxnum,maxden}];
];


Options[GetSliceIn] = {"NThreads"->Automatic, "PrimeNo"->0};
GetSliceIn[graphin_,varsin_,slicevar_,OptionsPattern[]]:=Module[{graphslice,psvalues,res,nthreads},
    nthreads = If[TrueQ[OptionValue["NThreads"]==Automatic],FFAutomaticNThreads[],OptionValue["NThreads"]];
    nthreads = If[EvenQ[nthreads],nthreads,nthreads-1];  (* round to even *)
    FFNewGraph[graphslice];
    FFGraphInputVars[graphslice,slicevarin,{slicevar}];
    psvalues = Table[RandomInteger[{10^4,10^8}],{ii,Complement[varsin,{slicevar}]}];
    FFAlgRatFunEval[graphslice,psvaluesnode,{slicevarin},{slicevar},varsin /. Rule@@@Transpose[{Complement[varsin,{slicevar}],psvalues}]];
    FFAlgSimpleSubgraph[graphslice,slicecoeffs,{psvaluesnode},graphin];
    FFGraphOutput[graphslice,slicecoeffs];
    res = FFParallelReconstructUnivariateMod[graphslice,{slicevar},
        {"StartingPrimeNo"->OptionValue["PrimeNo"], "PrintDebugInfo"->0,
       "NThreads"->nthreads, 
       "MaxPrimes"->200,
       "MinDegree"->Max[{nthreads/2-3,2}],
       "DegreeStep"->nthreads/2,
       "MaxDegree"->300}];
    FFDeleteGraph[graphslice];
    Return[{Rule@@@Transpose@({Complement[varsin,{slicevar}], psvalues}),res}];
  ];


Options[MatchCoefficientFactors2]={"PrimeNo"->0};
MatchCoefficientFactors2[slicedcoeffs_List, slicedfactorguess_List,OptionsPattern[]]:=Module[
{allfactors,factorguessclist,factorguessass,slicedcoeffsclist,slicedcoeffsclistass,
   eqs,sol,lhs,cs,rhss,slicedfactorguessnonconst,T},
  
  If[(Variables[slicedcoeffs]=!={xx})&&(Variables[slicedcoeffs]=!={}), Print["error: the sliced coefficients should depend on xx only"]; Abort[]];
  
  slicedfactorguessnonconst = DeleteCases[slicedfactorguess, zz_/;Variables[zz[[1]]]==={}];

  factorguessclist = DeleteCases[FactorList[#,"Modulus"->FFPrimeNo[OptionValue["PrimeNo"]]], a_/;Variables[a]==={}]&/@slicedfactorguessnonconst[[All,2]];
  allfactors = Union[DeleteCases[Flatten[factorguessclist],a_/;Variables[a]==={}]];
  factorguessass = Table[Association[Rule@@#&/@cc],{cc,factorguessclist}];

  slicedcoeffsclist = Map[DeleteCases[FactorList[#,"Modulus"->FFPrimeNo[OptionValue["PrimeNo"]]], a_/;Variables[a]==={}]&,slicedcoeffs];

  slicedcoeffsclistass = Table[Association[(Rule@@#)&/@scf], {scf,slicedcoeffsclist}];
  cs = Array[T,Length[slicedfactorguessnonconst]];
  lhs = Table[cs . (#[fac]&/@factorguessass/._Missing->0), {fac,allfactors}];
  rhss = Table[Table[(scfa[fac]/._Missing->0), {fac,allfactors}],{scfa,slicedcoeffsclistass}];
  sol = Flatten[Quiet[Solve[lhs-#==0]]]&/@rhss;
  
  Which[ 
    MemberQ[sol,{}], 
    Print["No solution found"]; Return[{}],
   
    Variables[cs/.sol]=!={},
    Print["non-unique solution, setting unconstrained coefficients to zero"];
    Return[Times@@(slicedfactorguessnonconst[[All,1]]^#) &/@(cs/.sol/.T[_]:>0)],    
   
    Variables[cs/.sol]==={},
    Return[Times@@(slicedfactorguessnonconst[[All,1]]^#) &/@(cs/.sol)],
    
    True,
    Return[$Failed]
  ];
];


ConstructAnsatzX[cnormfactored_,factors_List,numdeg_,x_] := Module[{tmp,den,factlist,facts,degfact,expfact,
   guessednumdeg,factorsnox,dendeg,denfactors,ansatzterms},
    tmp = Together[cnormfactored];
    
    den = Denominator[tmp];
    guessednumdeg = Exponent[Numerator[tmp] /. factors,x];
    
    (* We set to 1 all factors which do not depend on x *)
    factorsnox = Select[factors, FreeQ[Variables[#[[2]]],x]&];
    factorsnox = factorsnox[[All,1]]->1//Thread;

    dendeg = Exponent[den /. factors,x];
    den = den /. factorsnox;
    factlist = Select[FactorList[den],Variables[#]=!={}&];
    
    expfact = factlist[[All,2]];
    facts = factlist[[All,1]];
    degfact = Exponent[facts /. factors,x];

    ansatzterms = Table[ x^i0/facts[[i1]]^i2,{i1,1,Length[facts]},{i0,0,degfact[[i1]]-1},{i2,0,expfact[[i1]]}]//Flatten;
    
    If[guessednumdeg+numdeg>dendeg, 
       ansatzterms = Join[ansatzterms, Table[x^ii, {ii,1,guessednumdeg+numdeg-dendeg}]]; ];
    
    ansatzterms = Append[DeleteDuplicates@DeleteCases[ansatzterms,1],1]; (* 1 must be at the end in FFLinearFit *)

    ansatzterms = ansatzterms/cnormfactored;
    
    Return[ansatzterms]
];


Options[ReconstructFunctionApart]={"PrimeNo"->0, "NThreads"->Automatic, "PrintDebugInfo"->0, "CoefficientAnsatzResidues"->False};
ReconstructFunctionApart[graphin_, varsin_List, X_, coefficientansatz_List, OptionsPattern[]] := Module[
  {graph,slicerules,coeffactorguess,
  coeffansatztwist,slicedfactors,coeffnorm,dendegreesX,numdegreesX,slicerulesX,slicedX,coeffnormfactored,fitgraph,allmt,sliced,now,
  factors,ansaetze,lengthansaetze,outputelements,fitentries,fitcoefficients,allmtbutX,fitlearn,recfit,solfit,
  outcoeffs,allcoeffsout,sort,rndpoint,Print1,Print2,extrafactorsguess,tmpsol,newcoeffactorguess,
  totaldegrees,tmpslicerules,tmpsliced,maxmindegrees,alldegrees,zeovars,check},

  If[!MemberQ[varsin,X], Print["cannot apart w.r.t. ", X]; Abort[];,
     allmt = Prepend[DeleteCases[varsin,X], X];];
            
  Print1[strings__] := If[OptionValue["PrintDebugInfo"]>0, Print[strings]]; 
  Print2[strings__] := If[OptionValue["PrintDebugInfo"]>1, Print[strings]];
  
  coeffansatztwist = Select[Factor@coefficientansatz, Complement[Variables[#],varsin]==={} &]; 

  FFDeleteGraph[graph];
  FFNewGraph[graph,in,allmt];

  (* Sort variables. The variable we partial-fraction with respect to must be first *)
  FFAlgRatFunEval[graph,sortedvars,{in},allmt,varsin];
  FFAlgSimpleSubgraph[graph,coefficients,{sortedvars},graphin];
  FFGraphOutput[graph, coefficients];
  
  Print1["computing univariate slice in ", allmt]; now = AbsoluteTime[];
  {slicerules, sliced} = GetSlice[graph, allmt,{"PrimeNo"->OptionValue["PrimeNo"], "NThreads"->OptionValue["NThreads"]}];
  Print1["-> ", AbsoluteTime[]-now, " s"];
  Print1["degrees before normalisation {max num., max den.}", GetDegreeMax@sliced];

  Print1["matching coefficient factors"];
  coeffactorguess = FindIndependentFactorsNumerical[coeffansatztwist][[2,;;,2]];
  coeffactorguess = Rule[#,Factor[#/.slicerules,Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]]&/@coeffactorguess;
  coeffnorm = MatchCoefficientFactors2[sliced,coeffactorguess,"PrimeNo"->OptionValue["PrimeNo"]]; 
  Print1["degrees after normalisation {max num., max den.}", GetDegreeMax@Factor[(sliced/coeffnorm/.slicerules),Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]];
  FFAlgRatFunEval[graph,norm,{in},allmt,Together[1/coeffnorm]]//Print1;
  FFAlgMul[graph,normcoeffs,{coefficients,norm}]//Print1;
  FFGraphOutput[graph,normcoeffs];
  
  Print1["computing univariate slice in ", X];
  now = AbsoluteTime[];
  {slicerulesX, slicedX} = GetSliceIn[graph,allmt,X,{"PrimeNo"->OptionValue["PrimeNo"], "NThreads"->OptionValue["NThreads"]}];
  Print1["-> ", AbsoluteTime[]-now, " s"];
  Print2["check denominators: ", Union[Denominator[slicedX]]==={1}];

  dendegreesX = Exponent[Denominator[Together[coeffnorm /. slicerulesX]], X];
  numdegreesX = Exponent[slicedX, X];
    
  Print1["finding independent factors"];
  {coeffnormfactored, factors} = FindIndependentFactorsNumerical[coeffnorm];
  
  Print1["constructing ansaetze for partial fractions"];
  ansaetze = Table[ConstructAnsatzX[coeffnormfactored[[Kk]],factors,numdegreesX[[Kk]],X],{Kk,Length[coeffnormfactored]}];
  lengthansaetze = Length/@ansaetze;

  FFAlgRatFunEval[graph,ys,{in},allmt, Append[factors[[All,2]],X]];
  FFAlgRatFunEval[graph,ansatz,{ys},Append[factors[[All,1]],X],Flatten[ansaetze]];
  FFAlgChain[graph,eqs,{ansatz,normcoeffs}];
  FFGraphOutput[graph,eqs];
  
  (* preparing pattern for multi-fit *)
  outputelements = Flatten[Join[Table[ans[i1,i2], {i1,1,Length[coeffnormfactored]},{i2,1,lengthansaetze[[i1]]}],
    Array[r,Length[coeffnormfactored]]]];
  Print1[FFNParsOut[graph,eqs]-Length[outputelements]===0];

  fitentries = Table[ Append[Table[ans[i1,i2],{i2,lengthansaetze[[i1]]}], r[i1]], {i1,Length[coeffnormfactored]}];
  fitcoefficients = Table[c[i1,i2], {i1,Length[coeffnormfactored]},{i2,lengthansaetze[[i1]]}];

  (* Form of the equations for the fit: *)
  (*Append[fitcoefficients[[1]],0].fitentries[[1]] == fitentries[[1,-1]]*)
  
  Print1["preparing multi-fit graph"];
  allmtbutX = DeleteCases[allmt,X];
  FFDeleteGraph[fitgraph];
  FFNewGraph[fitgraph,in,allmtbutX];
  FFAlgSubgraphMultiFit[fitgraph,multifit,{in},graph,{X},outputelements->fitentries];
  FFGraphOutput[fitgraph,multifit];
  
  Print1["learning"]; now = AbsoluteTime[];
  fitlearn = FFMultiFitLearn[fitgraph,fitcoefficients];
  zerovars=Flatten["ZeroVars"/.fitlearn];
  Print1["-> ", AbsoluteTime[]-now, " s"];
  Print2["check learn output: ", And@@(#[[0]]==List&/@fitlearn)];
  Print2["check indep. variables: ", Union["IndepVars" /. fitlearn]==={{}}];
  
     (* Print["reconstructing... this may take some time"]; now = AbsoluteTime[];
    recfit = FFReconstructFunction[fitgraph,allmtbutX,{"NThreads"->OptionValue["NThreads"],"PrintDebugInfo"->OptionValue["PrintDebugInfo"]}];   
    Print["-> ", AbsoluteTime[]-now, " s"];*)

   
   recfit = Module[{slicerules2,sliced2,coeffactorguess2,slicedfactors2,coeffnorm2,recfit2},
      Print1["computing univariate slice in ", allmtbutX]; now=AbsoluteTime[];
      {slicerules2, sliced2} = GetSlice[fitgraph, allmtbutX,{"PrimeNo"->OptionValue["PrimeNo"], "NThreads"->OptionValue["NThreads"]}];
      Print1["->", AbsoluteTime[]-now, " s"];

      Print1["degrees before normalisation {max num., max den.}", GetDegreeMax@sliced2];
        
            If[OptionValue["CoefficientAnsatzResidues"],
        (* Guess spurious factors in the denominators *)
        Print1["enhancing factor guess"]; now=AbsoluteTime[];    
        extrafactorsguess = {};
        For[ii=1,ii<=Length[coeffactorguess],ii++,
          If[Exponent[coeffactorguess[[ii,1]],X]===1,
            tmpsol = Flatten@Solve[coeffactorguess[[ii,1]]==0,X];
            extrafactorsguess = Union[extrafactorsguess,Factor[coeffactorguess[[All,1]]/.tmpsol]];
            Clear[tmpsol];
          ];
        ];
        coeffactorguess2 = Select[Union[coeffactorguess[[All,1]],extrafactorsguess],FreeQ[Variables[#], X]&];
        coeffactorguess2 = FindIndependentFactorsNumerical[coeffactorguess2][[2,All,2]];
        Print1[Length[coeffactorguess2], " independent factors"];,
        coeffactorguess2 = Select[coeffactorguess[[All,1]], FreeQ[Variables[#], X]&];
      ];

        
      Print1["guessing factors"];      
      coeffactorguess2=Rule[#,Factor[#/.slicerules2,Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]]&/@coeffactorguess2;
      coeffnorm2 = MatchCoefficientFactors2[sliced2,coeffactorguess2,"PrimeNo"->OptionValue["PrimeNo"]];  
      Print1["degrees after normalisation {max num., max den.}", GetDegreeMax@Factor[(sliced2/coeffnorm2/.slicerules2),Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]];
     
      FFAlgRatFunEval[fitgraph,norm,{in},allmtbutX,Together[1/coeffnorm2]]//Print1;
      FFAlgMul[fitgraph,normcoeffs,{multifit,norm}]//Print1;
      FFGraphOutput[fitgraph,normcoeffs];
      
        (* let FiniteFlow compute the degrees *)
         Print1["reconstructing... this may take some time"]; now = AbsoluteTime[];
         recfit2 = FFReconstructFunction[fitgraph,allmtbutX,{"PrintDebugInfo"->OptionValue["PrintDebugInfo"],"NThreads"->OptionValue["NThreads"]}]; 
         Print1["-> ", AbsoluteTime[]-now, " s"];

      recfit2 = coeffnorm2*recfit2;
      recfit2
    ];
      
  solfit = Join@@FFMultiFitSol[recfit,fitlearn];
  allcoeffsout = Table[ Factor[fitcoefficients[[Kk]] /. solfit] . (coeffnormfactored[[Kk]]*ansaetze[[Kk]] /. factors),
      {Kk,1,Length[coeffnormfactored]}];
  Print2["check variables: ", Variables[allcoeffsout]===Union[allmt]];
  
  FFDeleteGraph[fitgraph];
  FFDeleteGraph[graph];
  
  rndpoint = Thread[varsin->RandomInteger[{99,99999999999},Length[varsin]]];

  check = Union[FFGraphEvaluate[graphin, varsin /. rndpoint,"PrimeNo"->OptionValue["PrimeNo"]+1]-
       FFRatMod[allcoeffsout /. rndpoint, FFPrimeNo[OptionValue["PrimeNo"]+1]]];
  If[check=!={0}, Print["check failed!"]]; 

  Print1["MaxMemoryUsed = ", N[MaxMemoryUsed[]/10^9], " GB"];
  
  Return[allcoeffsout]
]


Iteration[expr_,wrt_]:=Module[{graph,in,out,vars,factorlist},
vars=Variables[expr];
factorlist=DeleteCases[First/@FactorList[Denominator[expr]], x_Integer];

(*Print[vars];
Print[factorlist];*)

FFDeleteGraph[graph];
FFNewGraph[graph, in, vars];
FFAlgRatExprEval[graph, out, {in}, vars, {expr}];
FFGraphOutput[graph, out];

ReconstructFunctionApart[
	graph,
	vars,
	wrt,
	factorlist,
	{"PrintDebugInfo"->0,"CoefficientAnsatzResidues"->True}
]//First
];


Test[expr_]:=Module[{tmp},
tmp=(#//Numerator//Expand//Length)&/@(List@@expr);
Print[{{"tot","max","len","ops"},{Total[tmp],Max[tmp],Length[tmp],LeafCount[expr]}}//TableForm];
];


expr=((((((((((((((((-1))*t+(-1))*t)*d+((7)*t+7)*t)*d+(((-16))*t+(-16))*t)*d+((12)*t+12)*t)*m12+(((((((-5/4))*t+(23/4)*s+3/4)*t+(7/4)*s+2)*t)*d+(((17/2)*t+((-79/2))*s+(-11/2))*t+((-23/2))*s+(-14))*t)*d+((((-19))*t+(89)*s+13)*t+(25)*s+32)*t)*d+(((14)*t+((-66))*s+(-10))*t+((-18))*s+(-24))*t)*m12+((((((((1)*s)*t+(2)*s)*t+(1)*s)*t^2)*d+((((((-11))*s+(-1/4))*t+((-63/4))*s+3/2)*t+(((-21/2))*s+(-17))*s+3/4)*t+(((-3))*s+3/4)*s+(-1))*t+((-3/2))*s^2)*d+(((((44)*s+3/2)*t+((1/2)*s+93/2)*s+(-10))*t+((73)*s+83)*s+(-9/2))*t+((49/2)*s+(-19/2))*s+7)*t+(14)*s^2)*d+((((((-76))*s+(-3))*t+(((-2))*s+(-61))*s+22)*t+(((-166))*s+(-158))*s+9)*t+(((-66))*s+31)*s+(-16))*t+((-42))*s^2)*d+(((((48)*s+2)*t+((2)*s+30)*s+(-16))*t+((124)*s+104)*s+(-6))*t+((58)*s+(-30))*s+12)*t+(40)*s^2)*m12+(((((((((-2))*s^2)*t+((-4))*s^2)*t+((-2))*s^2)*t^2)*d+((((((22)*s+(-8))*s+(-1/4))*t+((145/4)*s+(-23))*s)*t+(((25/4)*s+29)*s+(-63/4))*s+1/4)*t+(((1/4)*s+(-1/4))*s+(-35/4))*s)*t+(3)*s^2)*d+(((((((-88))*s+78)*s+5/2)*t+((((-1))*s+(-27 7/2))*s+197)*s)*t+((((-83/2))*s+(-153))*s+273/2)*s+(-5/2))*t+((((-1/2))*s+(-21/2))*s+139/2)*s)*t+((-28))*s^2)*d+((((((152)*s+(-246))*s+(-7))*t+(((4)*s+269)*s+(-572))*s)*t+(((87)*s+362)*s+(-393))*s+7)*t+((((-5))*s+65)*s+(-179))*s)*t+(84)*s^2)*d+(((((((-96))*s+252)*s+6)*t+((((-4))*s+(-218))*s+556)*s)*t+((((-58))*s+(-328))*s+374)*s+(-6))*t+(((10)*s+(-94))*s+150)*s)*t+((-80))*s^2)*m12+((((((((((1)*s+1)*s+(-3))*s)*t+(((2)*s+2)*s+(-6))*s)*t+(((1)*s+1)*s+(-3))*s)*t^2)*d+((((((((-11))*s+19/4)*s+177/4)*s+5/4)*t+((((-101/4))*s+43/2)*s+161/2)*s+1/2)*t+((((1)*s+(-113/4))*s+119/4)*s+99/2)*s+(-3/4))*t+((((13/2)*s+(-13/2))*s+27/2)*s+25/4)*s)*t+(((9/2)*s+3/2)*s+(-3/2))*s^2)*d+(((((((44)*s+(-207/2))*s+(-505/2))*s+(-19/2))*t+((((1/2)*s+141)*s+(-501/2))*s+(-438))*s+(-4))*t+(((((-13))*s+435/2)*s+(-461/2))*s+(-288))*s+11/2)*t+(((((-123/2))*s+141/2)*s+(-147/2))*s+(-97/2))*s)*t+((((-42))*s+(-14))*s+14)*s^2)*d+((((((((-76))*s+379)*s+633)*s+23)*t+(((((-2))*s+(-369))*s+726)*s+1092)*s+10)*t+((((48)*s+(-645))*s+485)*s+714)*s+(-13))*t+((((188)*s+(-238))*s+88)*s+123)*s)*t+(((126)*s+42)*s+(-42))*s^2)*d+(((((((48)*s+(-414))*s+(-578))*s+(-18))*t+((((2)*s+360)*s+(-662))*s+(-1012))*s+(-8))*t+(((((-52))*s+662)*s+(-270))*s+(-640))*s+10)*t+(((((-182))*s+262)*s+34)*s+(-102))*s)*t+((((-120))*s+(-40))*s+40)*s^2)*m12+(((((((((((-1))*s+1)*s+2)*s)*t+((((-2))*s+2)*s+4)*s)*t+((((-1))*s+1)*s+2)*s)*t^2)*d+(((((((7/2)*s+(-93/2))*s+(-49/2))*s+(-3/4))*t+((((6)*s+19/2)*s+(-387/4))*s+(-41))*s+(-3/4))*t+((((((-3/2))*s+14)*s+77/4)*s+(-135/2))*s+(-45/2))*s)*t+(((((-9/2))*s+(-1/2))*s+47/4)*s+(-41/4))*s^2)*t+((((-3))*s+(-15/2))*s+(-3/2))*s^3)*d+(((((((24)*s+370)*s+125)*s+11/2)*t+(((((-115/2))*s+(-35/2))*s+1479/2)*s+189)*s+11/2)*t+(((((14)*s+(-128))*s+(-383/2))*s+483)*s+108)*s)*t+((((42)*s+11/2)*s+(-140))*s+119/2)*s^2)*t+(((28)*s+70)*s+14)*s^3)*d+((((((((-130))*s+(-1064))*s+(-298))*s+(-13))*t+((((180)*s+52)*s+(-2037))*s+(-446))*s+(-13))*t+((((((-42))*s+386)*s+713)*s+(-1192))*s+(-252))*s)*t+(((((-126))*s+(-16))*s+505)*s+(-87))*s^2)*t+((((-84))*s+(-210))*s+(-42))*s^3)*d+(((((((160)*s+1036)*s+268)*s+10)*t+(((((-186))*s+(-94))*s+1906)*s+416)*s+10)*t+(((((40)*s+(-388))*s+(-854))*s+976)*s+228)*s)*t+((((120)*s+6)*s+(-576))*s+2)*s^2)*t+(((80)*s+200)*s+40)*s^3)*m12+((((((((((9/2)*s+87/4)*s+(-3/4))*s)*t+(((((-9/2))*s+43/4)*s+43)*s+(-3/4))*s)*t+((((3/2)*s+(-27/2))*s+(-5/4))*s+85/4)*s^2)*t+(((9/2)*s+(-6))*s+(-15/2))*s^3)*t+((3)*s+3)*s^4)*d+((((((((-42))*s+(-385/2))*s+11/2)*s)*t+((((43)*s+(-167/2))*s+(-379))*s+11/2)*s)*t+(((((-14))*s+127)*s+85/2)*s+(-373/2))*s^2)*t+((((-42))*s+56)*s+84)*s^3)*t+(((-28))*s+(-28))*s^4)*d+(((((((134)*s+565)*s+(-13))*s)*t+(((((-136))*s+215)*s+1108)*s+(-13))*s)*t+((((42)*s+(-392))*s+(-213))*s+543)*s^2)*t+(((126)*s+(-172))*s+(-294))*s^3)*t+((84)*s+84)*s^4)*d+((((((((-144))*s+(-550))*s+10)*s)*t+((((144)*s+(-182))*s+(-1076))*s+10)*s)*t+(((((-40))*s+400)*s+290)*s+(-526))*s^2)*t+((((-120))*s+176)*s+328)*s^3)*t+(((-80))*s+(-80))*s^4)*m12+(((((((3)*s^3)*t+(((-3/2))*s+3)*s^3)*t+((-3/2))*s^4)*t^2)*d+(((((-26))*s^3)*t+((14)*s+(-25))*s^3)*t+((14)*s+1)*s^3)*t^2)*d+((((72)*s^3)*t+(((-42))*s+66)*s^3)*t+(((-42))*s+(-6))*s^3)*t^2)*d+(((((-64))*s^3)*t+((40)*s+(-56))*s^3)*t+((40)*s+8)*s^3)*t^2))/((((d+(-9))*d+26)*d+(-24))*(s^2)*(((t+2)*t+1)*t)*((m12+(-1))*m12^2)*(((((-1))*m12+(3)*s+1)*m12+(((-3))*s+(-2))*s)*m12+((1)*s+1)*s^2)));


expr


test={d->13,m12->5,s->7,t->11}


Test[expr]


res1 = Iteration[expr, d]


expr==res1/.test


Test[res1]


res2 = Plus@@(Iteration[#, s]& /@ (List@@res1))


expr==res2/.test


Test[res2]


res3 = Plus@@(Iteration[#, t]& /@ (List@@res2))


expr==res3/.test


Test[res3]
